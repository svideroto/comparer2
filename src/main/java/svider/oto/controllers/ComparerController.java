package svider.oto.controllers;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Getter;
import lombok.Setter;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.service.ComparerService;

@EnableWebMvc
@Controller
public class ComparerController {

	@Autowired
	ComparerService comparerService;

	@Setter
	private static ArrayList<CurrencyTicket> currencyTicket;

	@Setter
	private static JSONArray JSONArray;

	@Getter
	private static File file = new File("C:\\Users\\OtaS\\workspace\\sevrlets\\comparer.txt");

	@Getter
	private static JSONParser jsonParser = new JSONParser();

	@RequestMapping("/")
		public ModelAndView getInitialPage(Map<String, Object> model)
		{	
			ModelAndView modelAndView = new ModelAndView();
			try {
				MyThread threadBanks = new MyThread(comparerService);
				threadBanks.start();
		
				if (JSONArray == null) {
					JSONArray = getData();
				}
				model.put("banks", JSONArray);
	
			} catch (IOException | ParseException e) {
				modelAndView = new ModelAndView("error-page");
				return modelAndView;
			}
			modelAndView = new ModelAndView("comparer");
			return modelAndView;
		}

	// reads and returns data from local file, also for ajax
	@RequestMapping("/getData")
	@ResponseBody
	public JSONArray getData() throws IOException, ParseException {

		FileReader reader = new FileReader(getFile());

		Object obj = jsonParser.parse(reader);

		JSONArray JSONArrayBanks = (JSONArray) obj;

		reader.close();

		return JSONArrayBanks;
	}
}

class MyThread extends Thread {

	private ComparerService comparerService;

	private final long timesADay = 3;
	// thread repeats 3 times a day
	private final long PERIOD = timesADay * 60 * 60 * 1000;

	// work thread
	public MyThread(ComparerService comparerService) {

		this.comparerService = comparerService;

		// date "nula" - first start during the day, from that moment it counts
		// 24 hours/3 = 8 hours, next update takes on 08:01(if date is 0,1)
		LocalDateTime date = LocalDateTime.of(LocalDate.now(), LocalTime.of(12, 51));

		Instant instantTime = Timestamp.valueOf(date).toInstant();

		Timer timer = new Timer();

		timer.scheduleAtFixedRate(myTask, Date.from(instantTime), PERIOD);
	}

	TimerTask myTask = new TimerTask() {

		ObjectMapper objectMapper = new ObjectMapper();

		File file = ComparerController.getFile();

		JSONParser jsonParser = ComparerController.getJsonParser();

		@Override
		public void run() {
			try {

				ArrayList<CurrencyTicket> currencyTickets = comparerService.getAllCurrencyTickets();

				ComparerController.setCurrencyTicket(currencyTickets);

				String banks = objectMapper.writeValueAsString(currencyTickets);

				currencyTickets.clear();

				if (file.exists()) {
					FileWriter fileWriter = new FileWriter(file);
					fileWriter.write(banks);
					fileWriter.close();
					Object obj = jsonParser.parse(banks);
					ComparerController.setJSONArray((JSONArray) obj);
				}

			} catch (IOException | InterruptedException | ParseException e) {
				e.printStackTrace();
			}

		}
	};
}
