package svider.oto.comparer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComparerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComparerApplication.class, args);
	}

}
