package svider.oto.service;

import org.springframework.stereotype.Component;

import svider.oto.currencyticket.CurrencyTicket;

@Component
public interface IBankComparer {
	public CurrencyTicket downloadRates() throws InterruptedException;
}
