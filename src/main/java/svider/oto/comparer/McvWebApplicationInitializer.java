package svider.oto.comparer;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

//for inicialization the Servlet Dispatcher
public class McvWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

	@Override
	protected Class<?>[] getRootConfigClasses() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {MvcWebConfig.class};
	}

	@Override
	protected String[] getServletMappings() {
		
		return new String[] { "/" };
	}

}
