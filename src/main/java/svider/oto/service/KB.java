package svider.oto.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import svider.oto.connector.selenium.ConnectorOfSelenium;
import svider.oto.currencyticket.Currencies;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Component
public class KB extends ConnectorOfSelenium implements IBankComparer {

	private static final String baseURL = "https://www.kb.cz/";
	private static final String urlParams = "cs/kurzovni-listek/cs/rl/index";
	private static String URL = baseURL + urlParams;
	private WebDriver webDriver;

	@Autowired
	CurrencyTicket currencyTicket;

	Currencies currencies;

	@Lookup
	public Currencies getCurrencies() {
		return null;
	}

	@Override
	public CurrencyTicket downloadRates() throws InterruptedException {
		webDriver = this.getWebDriver(webDriver, URL);

		WebElement downloadedData = Download();

		CurrencyTicket currencyTicket = Transform(downloadedData);

		closeWebDriver(webDriver);

		return currencyTicket;

	}

	public WebElement Download() throws InterruptedException {

		WebElement beforeAllRates = webDriver.findElement(By.cssSelector("div[class='text-center mt-4']"));

		JavascriptExecutor js = (JavascriptExecutor) webDriver;

		js.executeScript("arguments[0].scrollIntoView();", beforeAllRates);

		Thread.sleep(2000);
		beforeAllRates.click();
		Thread.sleep(2000);

		WebElement allRates = webDriver.findElement(By.cssSelector("div[class='grey-version bottom-space']"));

		return allRates;
	}

	public CurrencyTicket Transform(WebElement downloadedData) {

		currencyTicket.setAllRates(new ArrayList<Currencies>());

		List<WebElement> listDownloadedData = downloadedData.findElements(By.cssSelector("div[class='text-center']"));

		for (WebElement webElement : listDownloadedData) {

			currencies = this.getCurrencies();

			WebElement DownloadedDataCurrencyname = webElement.findElement(By.cssSelector(
					"div[class='exchange-tile__title montserat-black uppercase d-inline-flex align-items-center w-100 justify-content-center']"));

			int size = DownloadedDataCurrencyname.getText().trim().length();
			String Currencyname = DownloadedDataCurrencyname.getText().substring(size - 3, size);
			currencies.setCurrencyName(Currencyname);

			List<WebElement> listDownloadedDataBuyAndSell = webElement.findElements(By.tagName("tbody"));

			List<WebElement> numbersForBuyAndSell = listDownloadedDataBuyAndSell.get(1)
					.findElements(By.cssSelector("td[class='text-right']"));

			ArrayList<Double> arrayListDownloadedDataBuyAndSell = new ArrayList<Double>();

			for (WebElement webElementNumbersFouBuyAndSell : numbersForBuyAndSell) {
				String number = webElementNumbersFouBuyAndSell.getText().replace(",", ".");
				arrayListDownloadedDataBuyAndSell.add(Double.valueOf(number));
			}

			Collections.sort(arrayListDownloadedDataBuyAndSell);

			currencies.setBuy(arrayListDownloadedDataBuyAndSell.get(0));
			currencies.setSell(arrayListDownloadedDataBuyAndSell.get(1));

			currencyTicket.getAllRates().add(currencies);

		}

		WebElement myDateAndTime = downloadedData
				.findElement(By.cssSelector("div[class='exchange-timeswitch white-version box-shadow']"));

		WebElement myDate = myDateAndTime.findElement(By.className("control-group-inline"))
				.findElement(By.tagName("input"));

		String myDateString = myDate.getAttribute("value");

		String[] myDateStringArray = myDateString.split("\\.");

		int day = Integer.valueOf(myDateStringArray[0]);
		int month = Integer.valueOf(myDateStringArray[1]);
		int year = Integer.valueOf(myDateStringArray[2]);

		LocalDate myDateKB = LocalDate.of(year, month, day);

		List<WebElement> myTime = myDateAndTime.findElement(By.className("custom-select"))
				.findElements(By.tagName("option"));

		String myTimeString = null;

		for (WebElement webElementTime : myTime) {
			if (webElementTime.getAttribute("selected") != null) {

				myTimeString = webElementTime.getText();
			}
		}

		int myTimeCharactersSize = 0;
		int hours = 0;
		int minutes = 0;
		int seconds = 0;

		if (myTimeString != null) {
			myTimeCharactersSize = myTimeString.trim().length();

			hours = Integer.valueOf(myTimeString.substring((myTimeCharactersSize - 4), myTimeCharactersSize - 3));
			minutes = Integer.valueOf(myTimeString.substring((myTimeCharactersSize - 2), myTimeCharactersSize));

		}

		LocalTime myTimeKB = LocalTime.of(hours, minutes, seconds);
		LocalDateTime myDateAndTimeKB = LocalDateTime.of(myDateKB, myTimeKB);

		currencyTicket.setLastRefresh(myDateAndTimeKB);

		currencyTicket.setBank(EnumBanks.KB);
		currencyTicket.setNameOfBank("Komerční banka, a.s.");

		return currencyTicket;
	}
}
