package svider.oto.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import svider.oto.connector.selenium.ConnectorOfSelenium;
import svider.oto.currencyticket.Currencies;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Component
public class RB extends ConnectorOfSelenium implements IBankComparer {

	private static final String baseURL = "https://www.rb.cz/";
	private static final String urlParams = "informacni-servis/kurzovni-listek";
	private static String URL = baseURL + urlParams;
	private WebDriver webDriver;
	private static final String baseForDate = "https://www.rb.cz/";
	private static final String urlParamsForDate = "frontend-controller/backend-data/currency/listActual/v1?attachment=true&date=";
	private static final String date = LocalDate.now().toString();
	private static String URLForDate = baseForDate + urlParamsForDate + date;

	@Autowired
	CurrencyTicket currencyTicket;

	Currencies currencies;

	@Lookup
	public Currencies getCurrencies() {
		return null;
	}

	@Override
	public CurrencyTicket downloadRates() throws InterruptedException {

		webDriver = this.getWebDriver(webDriver, URL);

		WebElement downloadedData = Download();

		CurrencyTicket currencyTicket = null;
		try {
			currencyTicket = Transform(downloadedData);
		} catch (IOException e) {
			e.printStackTrace();
		}

		closeWebDriver(webDriver);

		return currencyTicket;

	}

	public WebElement Download() throws InterruptedException {

		WebElement allRates = webDriver
				.findElement(By.cssSelector("div[class='bg-default component-wrapper currencies ng-star-inserted']"));

		return allRates;
	}

	public CurrencyTicket Transform(WebElement downloadedData) throws IOException {

		currencyTicket.setAllRates(new ArrayList<Currencies>());

		downloadedData.findElement(By.tagName("tbody")).findElements(By.tagName("tr")).stream()
				.forEach(webElementTR -> {
					currencies = this.getCurrencies();
					String currencyName = webElementTR.findElement(By.cssSelector("td[class='code']")).getText();
					currencies.setCurrencyName(currencyName);

					List<Double> ListBuyAndSell = webElementTR.findElements(By.cssSelector("td[class='value']"))
							.stream().map(e -> e.getText().replace(",", ".")).map(e -> Double.valueOf(e)).sorted()
							.collect(Collectors.toList());

					currencies.setBuy(ListBuyAndSell.get(1));
					currencies.setSell(ListBuyAndSell.get(3));
					currencyTicket.getAllRates().add(currencies);

				});

		WebElement WebElementDateAndTimeRB = downloadedData.findElement(By.className("form-fields"));

		String myTimeStringRB = WebElementDateAndTimeRB.getText();

		int hours = 0;
		int minutes = 0;
		int seconds = 0;

		if (myTimeStringRB != null) {

			String[] myTimeStringArrayRB = myTimeStringRB.split(":");
			hours = Integer.valueOf(myTimeStringArrayRB[0]);
			minutes = Integer.valueOf(myTimeStringArrayRB[1]);

		}
		LocalTime myTimeRB = LocalTime.of(hours, minutes, seconds);

		Document doc = Jsoup.connect(URLForDate).get();
		String myDateStringRB = doc.selectFirst("effectiveDateFrom").text();

		String[] myDateArrayRB = myDateStringRB.split("T");
		String[] myDateArrayJustDateRB = myDateArrayRB[0].split("-");

		int day = Integer.valueOf(myDateArrayJustDateRB[2]);
		int month = Integer.valueOf(myDateArrayJustDateRB[1]);
		int year = Integer.valueOf(myDateArrayJustDateRB[0]);

		LocalDate myDateRB = LocalDate.of(year, month, day);
		LocalDateTime myDateAndTimeRB = LocalDateTime.of(myDateRB, myTimeRB);

		currencyTicket.setLastRefresh(myDateAndTimeRB);

		currencyTicket.setBank(EnumBanks.RB);
		currencyTicket.setNameOfBank("Raiffeisenbank a.s.");

		return currencyTicket;

	}
}
