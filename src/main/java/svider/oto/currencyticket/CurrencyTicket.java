package svider.oto.currencyticket;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@Scope("prototype")
public class CurrencyTicket {

	private EnumBanks bank;
	private ArrayList<Currencies> allRates;
	private LocalDateTime lastRefresh;
	private String nameOfBank;
}
