package svider.oto.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import svider.oto.connector.selenium.ConnectorOfSelenium;
import svider.oto.currencyticket.Currencies;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Component
public class CSOB extends ConnectorOfSelenium implements IBankComparer {

	private static final String baseURL = "https://www.csob.cz/";
	private static final String urlParams = "portal/lide/kurzovni-listek";
	private static String URL = baseURL + urlParams;
	private WebDriver webDriver;

	@Autowired
	CurrencyTicket currencyTicket;

	Currencies currencies;

	@Lookup
	public Currencies getCurrencies() {
		return null;
	}

	@Override
	public CurrencyTicket downloadRates() throws InterruptedException {
		webDriver = this.getWebDriver(webDriver, URL);

		List<WebElement> downloadedData = Download();

		CurrencyTicket currencyTicket = Transform(downloadedData);

		closeWebDriver(webDriver);

		return currencyTicket;
	}

	public List<WebElement> Download() throws InterruptedException {

		List<WebElement> WebElementsConnectionCSOB = webDriver.findElement(By.tagName("tbody"))
				.findElements(By.tagName("tr"));

		return WebElementsConnectionCSOB;

	}

	public CurrencyTicket Transform(List<WebElement> downloadedData) {

		currencyTicket.setAllRates(new ArrayList<Currencies>());

		for (WebElement webElementTD : downloadedData) {
			currencies = this.getCurrencies();
			WebElement buyElement = webElementTD.findElement(By.className("pui-buy"));
			String buyElementString = buyElement.findElement(By.className("npw-currency-type-change")).getText()
					.replace(",", ".");
			Double buyValue = Double.valueOf(buyElementString);
			currencies.setBuy(buyValue);
			WebElement sellElement = webElementTD.findElement(By.className("pui-sell"));
			String sellElementString = sellElement.findElement(By.className("npw-currency-type-change")).getText()
					.replace(",", ".");
			Double sellValue = Double.valueOf(sellElementString);
			currencies.setSell(sellValue);

			currencies.setCurrencyName(webElementTD.findElement(By.className("pui-currency")).getText());

			currencyTicket.getAllRates().add(currencies);

		}

		LocalDateTime myDateAndTimeCSOB = (LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0, 0)));

		currencyTicket.setLastRefresh(myDateAndTimeCSOB);

		currencyTicket.setBank(EnumBanks.CSOB);
		currencyTicket.setNameOfBank("Československá obchodní banka");

		return currencyTicket;

	}

}

	
