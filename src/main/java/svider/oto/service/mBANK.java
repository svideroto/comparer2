package svider.oto.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import svider.oto.connector.selenium.ConnectorOfSelenium;
import svider.oto.currencyticket.Currencies;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Component
public class mBANK extends ConnectorOfSelenium implements IBankComparer {

	private static final String baseURL = "https://www.mbank.cz/";
	private static final String urlParams = "kurzovni-listek/";
	private static String URL = baseURL + urlParams;
	private WebDriver webDriver;

	@Autowired
	CurrencyTicket currencyTicket;

	Currencies currencies;

	@Lookup
	public Currencies getCurrencies() {
		return null;
	}

	@Override
	public CurrencyTicket downloadRates() throws InterruptedException {

		webDriver = this.getWebDriver(webDriver, URL);

		WebElement downloadedData = Download();

		CurrencyTicket currencyTicket = Transform(downloadedData);

		closeWebDriver(webDriver);

		return currencyTicket;
	}

	public WebElement Download() throws InterruptedException {

		WebElement allRates = webDriver.findElement(By.className("table_0"));

		return allRates;
	}

	public CurrencyTicket Transform(WebElement downloadedData) {

		currencyTicket.setAllRates(new ArrayList<Currencies>());

		downloadedData.findElement(By.tagName("tbody")).findElements(By.tagName("tr")).stream()
				.filter(webElement -> !webElement.getAttribute("class").equals("chartbox")).forEach(webElement -> {

					currencies = this.getCurrencies();
					String currencyName = webElement.findElement(By.className("unit")).getText();
					currencies.setCurrencyName(currencyName);

					List<String> ListStrings = webElement.findElements(By.tagName("td")).stream()
							.filter(webElementTD -> webElementTD.getAttribute("class").equals(""))
							.map(webElementTD -> webElementTD.getText())
							.filter(webElementTD -> !(webElementTD.equals("100") || webElementTD.equals("1"))).sorted()
							.collect(Collectors.toList());

					currencies.setBuy(Double.valueOf(ListStrings.get(0)));
					currencies.setSell(Double.valueOf(ListStrings.get(2)));

					currencyTicket.getAllRates().add(currencies);
				});

		WebElement DateAndTimemBANK = webDriver.findElement(By.className("currenciesForm"));

		String DatemBANKString = DateAndTimemBANK.findElement(By.tagName("input")).getAttribute("value");

		int day = 0;
		int month = 0;
		int year = 0;

		if (DatemBANKString != null) {

			String[] DatemBANKArrayString = DatemBANKString.split("-");

			year = Integer.valueOf(DatemBANKArrayString[0]);
			month = Integer.valueOf(DatemBANKArrayString[1]);
			day = Integer.valueOf(DatemBANKArrayString[2]);

		}

		LocalDate localDatemBANK = LocalDate.of(year, month, day);

		List<WebElement> WebElementsTimemBANK = DateAndTimemBANK.findElement(By.tagName("select"))
				.findElements(By.tagName("option"));
		String myTimemBANKString = null;

		for (WebElement webElementTimemBANK : WebElementsTimemBANK) {
			if (webElementTimemBANK.getAttribute("selected") != null) {
				myTimemBANKString = webElementTimemBANK.getText();

			}
		}

		int hours = 0;
		int minutes = 0;
		int seconds = 0;

		if (myTimemBANKString != null) {
			String[] myTimemBANKArrayString = myTimemBANKString.split(":");

			hours = Integer.valueOf(myTimemBANKArrayString[0]);
			minutes = Integer.valueOf(myTimemBANKArrayString[1]);
		}

		LocalTime localTimemBANK = LocalTime.of(hours, minutes, seconds);

		LocalDateTime localDateTimemBANK = LocalDateTime.of(localDatemBANK, localTimemBANK);

		currencyTicket.setLastRefresh(localDateTimemBANK);

	    currencyTicket.setBank(EnumBanks.mBANK);
		currencyTicket.setNameOfBank("mBank S.A");

		return currencyTicket;
	}
}
