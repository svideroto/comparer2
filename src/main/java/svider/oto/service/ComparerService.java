package svider.oto.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Service;

import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Service
public class ComparerService {

	IBankComparer IBankComparer;

	CurrencyTicket currencyTicket;

	@Lookup
	public CurrencyTicket getCurrencyTicket() {
		return null;
	}

	@Autowired
	private Map<String, IBankComparer> requiredBank;

	ArrayList<CurrencyTicket> bankTickets = new ArrayList<CurrencyTicket>();

	public ArrayList<CurrencyTicket> getAllCurrencyTickets() throws IOException, InterruptedException {

		EnumSet<EnumBanks> banks = EnumSet.allOf(EnumBanks.class);
		for (EnumBanks bank : banks) {

			currencyTicket = getCurrencyTicket(bank);

			bankTickets.add(currencyTicket);

		}

		return bankTickets;
	}

	public CurrencyTicket getCurrencyTicket(EnumBanks bank) throws IOException, InterruptedException {

		IBankComparer IBankComparer = requiredBank.get(bank.toString());

		CurrencyTicket currencyTicket = IBankComparer.downloadRates();

		return currencyTicket;

	}
}
