package svider.oto.connector.selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

//creation of the Chromedriver, class for inharitance
public class ConnectorOfSelenium {

	protected WebDriver getWebDriver(WebDriver driver, String URL) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\OtaS\\Downloads\\selenium\\chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(URL);
		Thread.sleep(3000);
		return driver;
	}

	protected void closeWebDriver(WebDriver driver) {

		driver.close();
	}

}
