/**
 * 
 */

$(document).ready(function(){
	

	$('#table').hide();

	$('#button-hide').hide();
	
	$('#img-show').hide();

	
var banks = ${banks};

var bankMap;
	
const prefix = "/images/";
const suffix = ".png";

var filteredData =[];

function loadTableData(filteredData){

var dataHtml = '';

$('.tr-add').remove();


for (var bank of filteredData){
    dataHtml+= '<tr class="tr-add"><td>' + bank[0] + '</td><td class="center">' + bank[1] + '</td><td class="center">' + bank[2] + '</td></tr>';}

$('#tableData').append(dataHtml);
};



$('#button-show').click(function(){
	
	$.ajax({
		url : 'getData',
		success : function(data){
			banks = data;
			}
		}); 
	
bankMap = banks.map(bank => [bank.nameOfBank,bank.allRates]);

var innerText = $('#selectionOfCurrencies').val();

$('#author').hide();

const bodyImage = innerText.substring(0,3).toLowerCase();
const imagePath = prefix+bodyImage+suffix;

$('#img-show').attr("src",imagePath);

$('#img-show').show();

$('#chosentext').text(innerText);

$('#chosentext').show();


$('#button-hide').slideDown(600);

$('#divtry').show(2500);


innerText = innerText.substring(0,3);

filteredData = [];


bankMap.forEach(function(mainBank){
mainBank[1].forEach(function(bank){
if(bank.currencyName==innerText){
filteredData.push([mainBank[0],bank]);}
});
});


filteredData = filteredData.map(bank => [bank[0],bank[1].buy,bank[1].sell]);


loadTableData(filteredData);


$('#table').show();

});

	$('#thBuyUP').click(function(){
	
	filteredData = filteredData.sort((a,b) => a[1] - b[1])

	loadTableData(filteredData);
	});
	
	$('#thBuyDown').click(function(){
		
		filteredData = filteredData.sort((a,b) => b[1] - a[1])

		loadTableData(filteredData);
		});
	
	$('#thSellUP').click(function(){
		
		filteredData = filteredData.sort((a,b) => a[2] - b[2])

		loadTableData(filteredData);
		});
	
	$('#thSellDown').click(function(){
		
		filteredData = filteredData.sort((a,b) => b[2] - a[2])

		loadTableData(filteredData);
		});

	

$('#button-hide').click(function(){

$('#chosentext').hide();

$('#img-show').hide();

$('#button-hide').hide();

$('#author').show();

$('#table').hide();

});

});