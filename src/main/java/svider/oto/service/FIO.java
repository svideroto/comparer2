package svider.oto.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Lookup;
import org.springframework.stereotype.Component;

import svider.oto.connector.selenium.ConnectorOfSelenium;
import svider.oto.currencyticket.Currencies;
import svider.oto.currencyticket.CurrencyTicket;
import svider.oto.currencyticket.EnumBanks;

@Component
public class FIO extends ConnectorOfSelenium implements IBankComparer {

	private static final String baseURL = "https://www.fio.cz/";
	private static final String urlParams = "akcie-investice/dalsi-sluzby-fio/devizove-konverze";
	private static String URL = baseURL + urlParams;
	private WebDriver webDriver;

	@Autowired
	CurrencyTicket currencyTicket;

	Currencies currencies;

	@Lookup
	public Currencies getCurrencies() {
		return null;
	}

	@Override
	public CurrencyTicket downloadRates() throws InterruptedException {

		webDriver = this.getWebDriver(webDriver, URL);

		WebElement downloadedData = Download();

		CurrencyTicket currencyTicket = Transform(downloadedData);

		closeWebDriver(webDriver);

		return currencyTicket;
	}

	public WebElement Download() throws InterruptedException {

		WebElement WebElementsConnectionFIO = webDriver.findElement(By.id("page"));

		return WebElementsConnectionFIO;
	}

	public CurrencyTicket Transform(WebElement downloadedData) {

		currencyTicket.setAllRates(new ArrayList<Currencies>());

		downloadedData.findElement(By.tagName("tbody")).findElements(By.tagName("tr")).forEach(webElementFIO -> {

			currencies = this.getCurrencies();
			currencies.setCurrencyName(webElementFIO.findElement(By.className("col1")).getAttribute("innerHTML"));

			List<Double> ListDouble = webElementFIO.findElements(By.className("tright")).stream()
					.map(webElementFIOclassTright -> webElementFIOclassTright.getAttribute("innerHTML").replace(",",
							"."))
					.filter(StringFIO -> !(StringFIO.contains("%") || StringFIO.contains(" ") || StringFIO.equals("1")
							|| StringFIO.equals("100")))
					.map(String -> Double.valueOf(String)).sorted().collect(Collectors.toList());

			currencies.setBuy(ListDouble.get(0));
			currencies.setSell(ListDouble.get(2));

			currencyTicket.getAllRates().add(currencies);

		});

		WebElement WebElementDateFIO = downloadedData.findElement(By.name("keDni_den"));
		String myDateStringFIO = WebElementDateFIO.getAttribute("value");

		int day = Integer.valueOf(myDateStringFIO.substring(0, 2));
		int month = Integer.valueOf(myDateStringFIO.substring(3, 5));
		int year = Integer.valueOf(myDateStringFIO.substring(6, 10));
		LocalDate myDateFIO = LocalDate.of(year, month, day);

		List<WebElement> WebElementsTimeFIO = downloadedData.findElement(By.id("keDniCas"))
				.findElements(By.tagName("option"));
		String myTimeStringFIO = null;
		for (WebElement webElement : WebElementsTimeFIO) {
			if (webElement.getAttribute("selected") != null) {
				myTimeStringFIO = webElement.getAttribute("innerHTML");
			}
		}
		int hours = 0;
		int minutes = 0;
		int seconds = 0;

		if (myTimeStringFIO != null) {
			hours = Integer.valueOf(myTimeStringFIO.substring(0, 2));
			minutes = Integer.valueOf(myTimeStringFIO.substring(3, 5));

		}

		LocalTime myTimeFIO = LocalTime.of(hours, minutes, seconds);
		LocalDateTime myDateTimeFIO = LocalDateTime.of(myDateFIO, myTimeFIO);

		currencyTicket.setLastRefresh(myDateTimeFIO);

		currencyTicket.setBank(EnumBanks.FIO);
		currencyTicket.setNameOfBank("Fio banka, a.s.");

		return currencyTicket;

	}
}
